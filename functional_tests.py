#!/usr/bin/env python
from selenium import webdriver
import unittest, os

class NewVisitorTest(unittest.TestCase):
    def setUp(self):
        chromedriver_path = os.path.join(os.getcwd(), "closet/geckodriver")
        self.browser = webdriver.Firefox(executable_path="closet/geckodriver")

    def tearDown(self):
        self.browser.quit()


    def test_user_opens_site(self):

        # user goes to the site
        self.browser.get("http://localhost:8000")

        # sees Postcards in title
        print(self.browser.title)
        assert "Postcards" in self.browser.title

if __name__ == "__main__":
    unittest.main(warnings="ignore")