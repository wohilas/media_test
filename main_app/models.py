from django.db import models
import uuid

# Create your models here.
class Postcard(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=120)
    content = models.TextField()
    created = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __str__(self):
        return f"{self.id}: {self.created}"
    