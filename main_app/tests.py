from django.test import TestCase

from main_app.views import home_page
from django.urls import resolve


# Create your tests here.

class HomePageTest(TestCase):
    def test_resolve_home_page(self):
        found = resolve("/")
        self.assertEqual(found.func, home_page)